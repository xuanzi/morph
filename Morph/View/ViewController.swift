//
//  ViewController.swift
//  Morph
//
//  Created by YAP HAO XUAN on 12/09/2019.
//  Copyright © 2019 YAP HAO XUAN. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButton: UIButton!
    
    @IBAction func loginFunc(_ sender: Any) {
        loginViewModel.isValid.subscribe(onNext: {[unowned self] isValid in
            isValid ? print("true") : print("false")
        }, onError: nil, onCompleted: nil, onDisposed: nil )
    }
    
    var loginViewModel = LoginViewModel()
    
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        _ = emailTextField.rx.text.map { $0 ?? ""}.bind(to: loginViewModel.emailText)
        _ = passwordTextField.rx.text.map { $0 ?? ""}.bind(to: loginViewModel.passwordText)
        
    }
    

}



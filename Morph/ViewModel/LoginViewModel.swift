//
//  LoginViewModel.swift
//  Morph
//
//  Created by YAP HAO XUAN on 12/09/2019.
//  Copyright © 2019 YAP HAO XUAN. All rights reserved.
//

import Foundation
import RxSwift

struct LoginViewModel {
    
    var emailText = Variable<String>("")
    var passwordText = Variable<String>("")
    
    var isValid : Observable<Bool> {
        return Observable.combineLatest(emailText.asObservable() , passwordText.asObservable()) { email , password in
            
            email == "hi@gmail.com" && password == "123456"
            
        }
    }
}
